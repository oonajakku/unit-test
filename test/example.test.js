const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        //initializatyion
        //create objects etc
        console.log("initializing tests");
    });
    it("Can add 1 and 2 together", () => {
        //tests
      expect(mylib.add(1, 2)).equal(3, "1 + 2 is not 3 for some reason?");  

    });
    after(() => {
        //cleanup
        //for example: shut express server down
        console.log("Testing completed!")
    });
});